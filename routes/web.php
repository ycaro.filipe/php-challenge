<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UploadfilesController@create');
Route::post('/process', 'UploadfilesController@process');
Route::post('/files-save', 'UploadfilesController@store');
Route::post('/files-delete', 'UploadfilesController@destroy');
Route::get('/files-delete/{id}', 'UploadfilesController@destroy_2');
Route::get('/files-show', 'UploadfilesController@index');
