# Rest API php-challenge

> Required

- PHP Version 7.2.7
- Composer
- Laravel 5.6

> How to install

1. Set up database credentials in .env file
2. Run `composer update`
3. Run `php artisan migrate` to migrate the database
4. Run `php artisan key:generate`
5. Run `php artisan serve` to start the local php serve