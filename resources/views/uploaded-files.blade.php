@extends('main')
 

@section('head')
    <link rel="stylesheet" href="{{ url('/css/custom.css') }}">
@endsection

@section('js')
    <script src="{{ url('/js/jquery.js') }}"></script>
    <script type="text/javascript">
        function process() {
            $.post({
                url: '/process',
                data: {_token: $('[name="_token"]').val()},
                dataType: 'json',
                success: function (data) {
                    alert("Files Processed Successfully!");
                    window.location = '/files-show';
                }
            });
        }
    
    </script>
@endsection
 
@section('content')
    <div class="table-responsive-sm">
    {{ csrf_field() }}
        <table class="table">
            <thead>
            <tr>
                <th scope="col"><button class='button-p' onclick='process()'>Process the Files</button>&nbsp Original Filename</th>
                <th scope="col">Filename</th>                
                <th scope="col">Processed</th>
                <th scope="col">Creation Date</th>
                <th scope="col">Action</th>
                
            </tr>
            </thead>
            <tbody>
            @foreach($files as $file)
                <tr>
                    <td>{{ $file->original_name }}</td>
                    <td>{{ $file->filename }}</td>                   
                    <td>{{ $file->processed }}</td>
                    <td>{{ $file->created_at }}</td>
                    <td>
                    <a type="button" href="/files-delete/{{$file->filename}}" class="btn button-p" >Remove file</a>                    
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection