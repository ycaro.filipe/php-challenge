<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Uploading files in Laravel with DropZone</title>
 
    <link rel="stylesheet" href="{{ url('/css/bootstrap.css') }}">
 
    @yield('head')
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{ url('/') }}">Upload</a>
        <span>/ &nbsp</span>
        <a class="navbar-brand" href="{{ url('/files-show') }}">View Uploaded Files</a>        
    </nav>
    <div class="container-fluid">
        @yield('content')
    </div>
 
    @yield('js')
 
</body>
</html>