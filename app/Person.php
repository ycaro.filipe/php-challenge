<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public function shiporders()
    {
        return $this->hasMany(Shiporder::class);
    }
}
