<?php

namespace App\Http\Controllers;
use App\Shiporder;
use App\Item;
use Illuminate\Http\Request;
use JWTAuth;

class ShiporderController extends Controller
{
    protected $user;
    public function __construct()
    {
        //$this->user = JWTAuth::parseToken()->authenticate();
        try {
            $this->user = JWTAuth::parseToken()->authenticate();
          } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            return response()->json(['error' => 'token_blacklisted'], 401);
          } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['error' => 'token_expired'], 401);
          }
    }
    public function index()
    {
        $shiporder = Shiporder::all();
       
        return $shiporder;
    }
    public function show($id)
    {
        $shiporder = Shiporder::find($id);
        if (!$shiporder) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, shiporder with id ' . $id . ' cannot be found'
            ], 400);
        }
        $shiporder->items = $shiporder->items()->get()->toArray();
        return $shiporder;
    }
    
}
