<?php

namespace App\Http\Controllers;
use App\Shiporder;
use App\Item;
use App\Person;
use Illuminate\Http\Request;
use JWTAuth;

class PersonController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }
    public function index()
    {
        return Person::all();
    }
    public function show($id)
    {
        $person =  Person::find($id);
        if (!$person) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, person with id ' . $id . ' cannot be found'
            ], 400);
        }
        return $person;
    }    
}
