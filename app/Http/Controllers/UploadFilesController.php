<?php
 
namespace App\Http\Controllers;
 
use App\Upload;
use App\Shiporder;
use App\Item;
use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
 
class UploadFilesController extends Controller
{
 
    private $files_path;
 
    public function __construct()
    {
        $this->files_path = public_path('/xmls');
    }
 
    /**
     * Display all of the XMLs.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = Upload::all();
        return view('uploaded-files', compact('files'));
    }
 
    /**
     * Show the form for creating uploading new files.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('upload');
    }
 
    /**
     * Saving files uploaded through XHR Request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $files = $request->file('file');
 
        if (!is_array($files)) {
            $files = [$files];
        }
 
        if (!is_dir($this->files_path)) {
            mkdir($this->files_path, 0777);
        }
 
        for ($i = 0; $i < count($files); $i++) {
            $file = $files[$i];
            $name = sha1(date('YmdHis') . str_random(30));
            $save_name = $name . '.' . $file->getClientOriginalExtension();
            
            $file->move($this->files_path, $save_name);
 
            $upload = new Upload();
            $upload->filename = $save_name;
            $upload->original_name = basename($file->getClientOriginalName());
            $upload->processed  = 'false';
            $upload->save();
        }
        return Response::json([
            'message' => 'File saved Successfully'
        ], 200);
    }
 
    /**
     * Remove the files from the storage.
     *
     * @param Request $request
     */
    public function destroy(Request $request)
    {
        $filename = $request->id;
        $uploaded_image = Upload::where('original_name', basename($filename))->first();
 
        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Sorry file does not exist'], 400);
        }
 
        $file_path = $this->files_path . '/' . $uploaded_image->filename;
       
        if (file_exists($file_path)) {
            unlink($file_path);
        }
 
        
        if (!empty($uploaded_image)) {
            $uploaded_image->delete();
        }
 
        return Response::json(['message' => 'File successfully delete'], 200);
    }
    public function destroy_2(Request $request)
    {
        $filename = $request->id;
        $uploaded_image = Upload::where('filename', basename($filename))->first();
 
        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Sorry file does not exist'], 400);
        }
 
        $file_path = $this->files_path . '/' . $uploaded_image->filename;
       
        if (file_exists($file_path)) {
            unlink($file_path);
        }
       
 
        if (!empty($uploaded_image)) {
            $uploaded_image->delete();
        }
 
        return redirect('/files-show');
    }

    public function process(){
        $files = Upload::where('processed', 'false')->get();
        $teste =[];
        
        foreach($files as $file){
        libxml_use_internal_errors(true);
           $xml = simplexml_load_file('xmls/'.$file->filename);   
           if ($xml === false) {
            echo "Failed loading XML\n";
            foreach(libxml_get_errors() as $error) {
                echo "\t", $error->message;
            }
        }       
           if (isset($xml->person)){                  
            foreach($xml->person as $reg){                 
                $person = new Person();
                $person->name = $reg->personname;
                $person->phone_1 = $reg->phones->phone[0];
                if(isset($reg->phones->phone[1])){
                    $person->phone_2 = $reg->phones->phone[1];
                }                                
                $person->save();                
            }            
           }elseif(isset($xml->shiporder)){
            foreach($xml->shiporder as $order){
                
                $shiporder = new Shiporder();
                $shiporder->person_id = $order->orderperson;
                $shiporder->shipto_name = $order->shipto->name;
                $shiporder->shipto_address = $order->shipto->address;
                $shiporder->shipto_city = $order->shipto->city;
                $shiporder->shipto_country = $order->shipto->country;
                $shiporder->save();
                
                foreach($order->items->item as $it){
                    $item = new Item();
                    $item->title = $it->title;
                    $item->note = $it->note;
                    $item->quantity = $it->quantity;
                    $item->price  = $it->price;
                    $shiporder->items()->save($item);
                }
            }
           }
           $file->processed = 'true';
           $file->update();
        }
        return Response::json("File processed Successfully", 200);
    }
}